import { ILogLevelChecker, IPredicate } from '../level/interfaces';
import { LogLevelMap } from '../level/types';
import { StreamTarget } from './types';
export interface ILoggerStyleMap {
    [key: string]: number[];
}
export interface ILoggerStyleMaps {
    operators: ILoggerStyleMap;
    types: ILoggerStyleMap;
}
export interface ILoggerConfig {
    label: string;
    time?: boolean;
    showLevel?: boolean;
    labelStyles?: number[];
    styleMaps?: Partial<ILoggerStyleMaps>;
    parent?: ILoggerConfig;
    logLevels?: ILogLevelChecker | Partial<LogLevelMap<boolean | IPredicate>>;
    /**
     * @name target
     * @type {Writable|string}
     * @description target stream to write log to; if set to filename, log is written to file
     * @default process.stdout
     */
    target?: StreamTarget | StreamTarget[];
}

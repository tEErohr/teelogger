/// <reference types="node" />
import { Writable } from 'stream';
export declare type StreamTarget = Writable | string;

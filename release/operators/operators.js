"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const styles = require("../style");
exports.operators = {
    s: {
        code: 's',
        test: /\%s/,
        format(value) {
            return value;
        }
    },
    c: {
        code: 'c',
        test: /\%([\d+|\;]{1,})c/,
        noArg: true,
        format(value, param, index) {
            const codes = param.split(';').map(code => parseInt(code));
            return styles.mapCodes(codes);
        }
    },
    o: {
        code: 'o',
        test: /\%o/,
        format(value) {
            return JSON.stringify(value, null, '  ');
        }
    },
    f: {
        code: 'f',
        test: /\%(\d*\.\d+)?f/,
        format(value, param, index) {
            param = param || '1.2';
            const [base = "1", _prec = "0"] = param.split('.');
            const prec = parseInt(_prec);
            const m = parseInt('1' + '0'.repeat(prec));
            value = parseInt(`${value * m}`, 10) / m;
            return `${value}`;
        }
    },
    d: {
        code: 'd',
        test: /\%d/,
        format(value) {
            return value;
        }
    }
};

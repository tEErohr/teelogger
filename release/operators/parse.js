"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const regex_1 = require("../regex");
const operators_1 = require("./operators");
exports.list = (() => Object.keys(operators_1.operators).map(key => {
    return operators_1.operators[key];
}))();
exports.re = regex_1.join(exports.list.map(op => op.test));
function testOperator(operator, value) {
    const re = regex_1.create(operator.test);
    return re.test(value);
}
exports.testOperator = testOperator;
function parse(pattern, args) {
    return pattern.replace(exports.re, (value, ...params) => {
        const operator = exports.list.find(op => testOperator(op, value));
        if (!operator) {
            throw new Error(`No operator for ${value}`);
        }
        //console.log( styles.wrap([1,3],'%s'), operator.test)
        //console.log( styles.mapCodes(44), value, styles.mapCodes(0))
        const arg = operator.noArg ? undefined : args.shift();
        return operator.format(arg, ...params);
    });
}
exports.parse = parse;

"use strict";
function __export(m) {
    for (var p in m) if (!exports.hasOwnProperty(p)) exports[p] = m[p];
}
Object.defineProperty(exports, "__esModule", { value: true });
const Format = require("./format");
exports.Format = Format;
const Style = require("./style");
exports.Style = Style;
const Operators = require("./operators");
exports.Operators = Operators;
const Config = require("./config");
exports.Config = Config;
const logger_1 = require("./logger");
__export(require("./parser"));
__export(require("./logger"));
function createLogger(config) {
    if ('string' === typeof config) {
        return createLogger({
            label: config,
            labelStyles: [1]
        });
    }
    return new logger_1.Logger(config);
}
exports.createLogger = createLogger;

"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Style = require("../style");
const regex_1 = require("../regex");
class Parser {
    constructor(operators, styleMaps = {}) {
        this.operators = operators;
        this.regex = regex_1.join(this.operators.map(op => op.test));
        this.styleMaps = {
            operators: styleMaps.operators || {},
            types: styleMaps.types || {}
        };
    }
    testOperator(operator, value) {
        const re = regex_1.create(operator.test);
        return re.test(value);
    }
    formatValueByType(value, valueType) {
        if (valueType in this.styleMaps.types) {
            const codes = this.styleMaps.types[valueType];
            value = Style.wrap(codes, value);
        }
        return value;
    }
    formatValue(value, operator) {
        if (operator.code in this.styleMaps.operators) {
            const codes = this.styleMaps.operators[operator.code];
            value = Style.wrap(codes, value);
        }
        return value;
    }
    parse(pattern, args) {
        return pattern.replace(this.regex, (value, ...params) => {
            const operator = this.operators.find(op => this.testOperator(op, value));
            if (!operator) {
                throw new Error(`No operator for ${value}`);
            }
            //console.log( styles.wrap([1,3],'%s'), operator.test)
            //console.log( styles.mapCodes(44), value, styles.mapCodes(0))
            let arg = operator.noArg ? undefined : args.shift();
            let formatted = operator.format(arg, ...params);
            formatted = this.formatValueByType(formatted, typeof arg);
            return this.formatValue(formatted, operator);
        });
    }
}
exports.Parser = Parser;

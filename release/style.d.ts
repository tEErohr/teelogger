export declare function underline(text: string): any;
export declare function bright(text: string): any;
export declare function dim(text: string): any;
export declare function invert(text: string): any;
export declare function italic(text: string): any;
export declare function mapCodes(color: number | number[]): any;
export declare function wrap(color: number | number[], value?: string, terminate?: number | false): any;

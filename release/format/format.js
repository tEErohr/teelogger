"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const util = require("util");
const Operators = require("../operators");
function format(pattern, ...args) {
    if ('string' !== typeof pattern) {
        return format('', pattern, ...args);
    }
    pattern = Operators.parse(pattern, args);
    return util.format(pattern, ...args);
}
exports.format = format;
function parsePattern(pattern, args) {
    return Operators.parse(pattern, args);
}
exports.parsePattern = parsePattern;

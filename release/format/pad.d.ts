export declare function lpad(value: string | any, length: number, fill: string[1]): any;
export declare function rpad(value: string | any, length: number, fill: string[1]): any;

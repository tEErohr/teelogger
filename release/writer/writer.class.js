"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class LogWriter {
    constructor(stdout = process.stdout) {
        this.stdout = stdout;
    }
    write(output) {
        return new Promise((resolve) => {
            let ok;
            ok = this.stdout.write(output, () => resolve(ok));
        });
    }
}
exports.LogWriter = LogWriter;

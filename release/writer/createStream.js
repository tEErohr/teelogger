"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const stream_1 = require("stream");
const filestreams_1 = require("./filestreams");
function createOutputStream(target) {
    if (Array.isArray(target)) {
        const stream = new stream_1.PassThrough();
        target.forEach(t => {
            stream.pipe(createOutputStream(t));
        });
        return stream;
    }
    if ('string' === typeof target) {
        return filestreams_1.createFileStream(target);
    }
    if (target instanceof stream_1.Writable) {
        return target;
    }
}
exports.createOutputStream = createOutputStream;

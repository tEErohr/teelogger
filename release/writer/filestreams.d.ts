/// <reference types="node" />
import { Writable, Transform } from 'stream';
export declare const cache: Map<string, Writable>;
export declare function stripColors<T extends string | Buffer>(value: T): T;
export declare function createColorTransform(output: Writable): Transform;
export declare function createFileStream(filepath: string): Writable;

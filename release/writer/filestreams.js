"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
const stream_1 = require("stream");
exports.cache = new Map();
function stripColors(value) {
    if ('string' === typeof value) {
        return stripColors(new Buffer(value)).toString('utf8');
    }
    if (value instanceof Buffer) {
        let out = new Buffer('');
        for (var i = 0; i < value.length; i++) {
            if (value[i] === 27 && value[i + 1] === 91) {
                i = value.indexOf(109, i + 2);
            }
            else {
                out = Buffer.concat([out, new Buffer([value[i]])]);
            }
        }
        return out;
    }
}
exports.stripColors = stripColors;
function createColorTransform(output) {
    const transform = new stream_1.Transform({
        transform(value, encoding, callback) {
            try {
                const out = stripColors(value);
                callback(null, out);
            }
            catch (e) {
                callback(e);
            }
        }
    });
    transform.pipe(output);
    return transform;
}
exports.createColorTransform = createColorTransform;
function createFileStream(filepath) {
    if (exports.cache.has(filepath) === false) {
        const stream = createColorTransform(fs.createWriteStream(filepath, 'utf8'));
        exports.cache.set(filepath, stream);
        stream.on('close', () => {
            exports.cache.delete(filepath);
        });
    }
    return exports.cache.get(filepath);
}
exports.createFileStream = createFileStream;
process.once('beforeExit', () => {
    exports.cache.forEach(w => {
        w.destroy();
    });
});

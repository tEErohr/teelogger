/// <reference types="node" />
import { Writable } from 'stream';
import { StreamTarget } from '../config';
export declare function createOutputStream(target: StreamTarget | StreamTarget[]): Writable;

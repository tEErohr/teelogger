import { LogLevel } from './types';
export interface IPredicate {
    (): boolean;
}
export interface ILogLevelChecker {
    (level: LogLevel): boolean;
}

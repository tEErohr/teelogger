"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.LOG_LEVEL_ERROR = 'error';
exports.LOG_LEVEL_WARN = 'warn';
exports.LOG_LEVEL_INFO = 'info';
exports.LOG_LEVEL_DEBUG = 'debug';
exports.LOG_LEVEL_TRACE = 'trace';

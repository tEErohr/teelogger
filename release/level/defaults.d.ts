import { IPredicate } from './interfaces';
import { LogLevelMap } from './types';
export declare const defaultMap: LogLevelMap<IPredicate>;
export declare function createPredicate(value: boolean | IPredicate): IPredicate;
export declare function createMap(map?: Partial<LogLevelMap<boolean | IPredicate>>): LogLevelMap<IPredicate>;

import { LogLevels } from './levels.enum';
export declare type LogLevel = string | keyof typeof LogLevels;
export declare type LogLevelMap<T> = {
    [K in LogLevel]: T;
};

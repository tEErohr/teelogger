import { Writable, PassThrough, Transform } from 'stream'
import * as path from 'path'
import * as fs from 'fs'

import { StreamTarget } from '../config'

import { createFileStream } from './filestreams'



export function createOutputStream ( target:StreamTarget|StreamTarget[] ):Writable {

  if ( Array.isArray(target) ) {
    const stream = new PassThrough()
    target.forEach ( t => {
      stream.pipe(createOutputStream(t))
    } )
    return stream
  }

  if ( 'string' === typeof target ) {
    return createFileStream(target)
  }

  if ( target instanceof Writable ) {
    return target
  }

}

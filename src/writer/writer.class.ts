import { Writable } from 'stream'

export class LogWriter {

  constructor ( private stdout:Writable=process.stdout ) {

  }


  write ( output:string|Buffer ):Promise<boolean> {
    return new Promise((resolve) => {
      let ok:boolean
      ok = this.stdout.write(output, () => resolve(ok) )
    })
  }


}

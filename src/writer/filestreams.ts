import * as fs from 'fs'
import { Writable, Transform } from 'stream'



export const cache:Map<string,Writable> = new Map()


export function stripColors <T extends string|Buffer> ( value:T ):T 
{
  if ( 'string' === typeof value ) {
    return stripColors ( new Buffer(value) ).toString('utf8') as T
  }

  if ( value instanceof Buffer ) {

    let out:Buffer = new Buffer('')

    for (var i = 0; i < value.length; i++) {
      if ( value[i] === 27 && value[i+1] === 91 ) {
        i = value.indexOf(109,i+2)
      } else {
        out = Buffer.concat([out,new Buffer([value[i]])])
      }
    }

    return out as T
  }
}

export function createColorTransform (output:Writable) {
  const transform = new Transform({
    transform <T extends string|Buffer>( value:T, encoding:string, callback:{(error:Error,data?:T)} ) {
      try {
        const out:T = stripColors<T>(value)
        callback(null,out)
      }catch(e){
        callback(e)
      }
    }
  })

  transform.pipe(output)
  return transform
}


export function createFileStream ( filepath:string ):Writable {

  if ( cache.has(filepath) === false ) {
    const stream = createColorTransform(fs.createWriteStream(filepath,'utf8'))
    cache.set(filepath,stream)
    stream.on('close',()=>{
      cache.delete(filepath)
    })
  }

  return cache.get(filepath)

}


process.once('beforeExit',()=>{
  cache.forEach(w => {
    w.destroy()
  })
})
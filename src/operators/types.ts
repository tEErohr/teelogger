import { operators } from './operators'

export type Operator = keyof typeof operators

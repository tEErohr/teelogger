import { IFormatOperator, IValueFormatter } from './interfaces'
import * as styles from '../style'
import { join, create, match } from '../regex'
import { operators } from './operators'


export const list:IFormatOperator<any,any>[] = (()=>Object.keys(operators).map ( key => {
  return operators[key]
} ))()

export const re = join(list.map(op => op.test))


export function testOperator<O extends string[1],T> ( operator:IFormatOperator<O,T>, value:string ) {

  const re = create(operator.test)
  return re.test(value)

}

export function parse ( pattern:string, args:any[] ) {

  return pattern.replace ( re, ( value:string, ...params:any[] ) => {
    const operator = list.find ( op => testOperator(op,value) )
    if ( !operator ) {
      throw new Error(`No operator for ${value}`)
    }
    //console.log( styles.wrap([1,3],'%s'), operator.test)
    //console.log( styles.mapCodes(44), value, styles.mapCodes(0))
    const arg = operator.noArg ? undefined : args.shift()
    return operator.format(arg,...params)
  } )
  

}
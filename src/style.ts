
export function underline ( text:string ) {
  return wrap(4,text)
}

export function bright ( text:string ) {
  return wrap(1,text)
}

export function dim ( text:string ) {
  return wrap(2,text)
}

export function invert ( text:string ) {
  return wrap(7,text)
}

export function italic ( text:string ) {
  return wrap(3,text)
}

export function mapCodes ( color:number|number[] ) {

  if ( !Array.isArray(color) ) {
    
    return mapCodes ( [color] )

  }
  
  return '\x1b[' + color.join(';') + 'm'

}

export function wrap ( color:number|number[], value?:string, terminate?:number|false ) {

  let out = mapCodes(color)

  if ( !value ) {
    return out
  }

  out += value

  if ( 'undefined' === typeof terminate ) {
    terminate = 0
  }

  if ( 'number' === typeof terminate ) {
    out += mapCodes(terminate)
  }
  return out

}

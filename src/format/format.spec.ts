import 'mocha'
import { expect } from 'chai'

import { format, parsePattern } from './format'


const TEST_PATTERN = 'Hello %s, I need %1.3f seconds, and %d minutes. %o'
const TEST_PARAMS = [
  "World",
  32.4234244,
  5,
  {bla: "Foo"}
]

describe(`Test format`,function(){
  
  describe ( `parsePattern`, () => {

    const parsed = parsePattern(TEST_PATTERN,TEST_PARAMS)

    before((done)=>{

      console.log('parsed',parsed)

      done()

    })

    it(`TEST_PARAMS.length === 0`,()=> {

      expect(TEST_PARAMS).to.have.lengthOf(0)

    })

  } )

})


import { lpad } from './pad'

export function now () {
  const t = new Date()
  return t.toLocaleTimeString() + '.' + lpad(t.getTime()%1000,3,'0')
}
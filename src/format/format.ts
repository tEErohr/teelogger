import * as util from 'util'
import { join, match } from '../regex'
import * as Operators from '../operators'


export function format ( pattern:string|any, ...args:any[] ) {

  if ( 'string' !== typeof pattern ) {
    return format ( '', pattern, ...args )
  }

  pattern = Operators.parse ( pattern, args )
  return util.format(pattern, ...args)

}

export function parsePattern ( pattern:string, args:any[] ) {

  return Operators.parse(pattern,args)

}

export function create ( source:string|RegExp ) {

  if ( 'string' === typeof source ) {
    return new RegExp(source)
  }

  return source
}

export function join ( pattern:(RegExp|string)[] ) {

  let rg_sources:string[] = []

  for (var i = 0; i < pattern.length; i++) {
    const current = create(pattern[i])
    rg_sources.push(current.source)
  }

  const src = rg_sources.join('|')
  const rg = new RegExp(src,'gm')
  return rg

}

export function match ( value:string, regex:RegExp ) {

   let matches:RegExpExecArray[]=[]

   let match:RegExpExecArray
   let pos:number = -1

   while ( (match = regex.exec(value)) && !!match && match.index > pos ) {

     pos = match.index
     matches.push(match)

   }

   return matches

}
import 'mocha'
import { expect } from 'chai'

import { join, match } from './regex'

describe(`Test regex`,function(){
  
  describe ( `join`, () => {

    const splitter = join(['foo','bar',/\d+/])
    const TEST_TEXT = 'This foo is a bar of 50 feet length.'

    let matches:RegExpExecArray[]
    let parts:string[] = TEST_TEXT.split(splitter)

    before((done)=>{

      matches = match(TEST_TEXT,splitter)
      console.log('splitter',splitter)
      console.log('matches',matches)
      console.log('parts',parts)
      done()
    })

    it(`has matches`,()=> {
      expect(matches).to.have.length.greaterThan(0)
    })

    it('4 parts', () => {
      expect(parts).to.have.lengthOf(4)
    })

  } )

})


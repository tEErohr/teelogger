import * as Format from '../format'
import * as Style from '../style'
import * as Operators from '../operators'
import * as Config from '../config'
import { Parser } from '../parser/parser.class'
import * as util from 'util'

import { LogLevel, LogLevels, ILogLevelChecker, IPredicate, createMap } from '../level'
import { LogWriter, createOutputStream } from '../writer'

export class Logger {

  constructor ( private config:Config.ILoggerConfig, private parent?:Logger ) {

  }

  protected writer=new LogWriter(createOutputStream(this.config.target))

  readonly label:string = Style.wrap(this.config.labelStyles,this.config.label,0)

  protected labelsFromRoot:string[]=this.parent ? [...this.parent.labelsFromRoot, this.label] : [this.label]

  
  readonly parser=new Parser(Operators.list,this.config.styleMaps)


  protected write ( data:string ) {
    this.writer.write(data)
  }

  protected getLogLevelPredicate ( logLevel:LogLevel ):IPredicate {
    const logLevels = this.config.logLevels


    if ( 'function' === typeof logLevels ) {
      return () => logLevels(logLevel)
    }

    const map = createMap ( logLevels )
    return map[logLevel]
  }

  protected logLevel ( level:LogLevel, format:string|any, args:any[] ) {
    const predicate = this.getLogLevelPredicate(level)
    if ( predicate() === false ) {
      return
    }

    const formatted = this.format(format, ...args)
    
    if ( this.config.showLevel !== false ) {
      this.write('[' + level.toUpperCase() + ']' )
    }

    if ( this.config.time !== false ) {
      this.write(Style.dim(Format.now())+' ')
    }
    
    this.write ( this.labelsFromRoot.join('|') + ' ' )
    this.write ( formatted )
    this.write('\n')
  }


  format ( pattern:string|any, ...args:any[] ) {
    if ( 'string' !== typeof pattern ) {
      return this.format ( '', pattern, ...args )
    }
    const format = this.parser.parse ( pattern, args )
    const formatted = util.format(format, ...args)
    return formatted
  }

  log ( pattern:string|any, ...args:any[] ) {
    this.logLevel('info',pattern,args)
  }

  error ( pattern:string|any, ...args:any[] ) {
    this.logLevel('error',pattern,args)
  }

  info ( pattern:string|any, ...args:any[] ) {
    this.logLevel('info',pattern,args)
  }

  warn ( pattern:string|any, ...args:any[] ) {
    this.logLevel('warn',pattern,args)
  }

  debug ( pattern:string|any, ...args:any[] ) {
    this.logLevel('debug',pattern,args)
  }

  trace ( pattern:string|any, ...args:any[] ) {
    this.logLevel('trace',pattern,args)
  }

  defaultChildConfig ( options:string|Config.ILoggerConfig ):Config.ILoggerConfig {
    if ( 'string' === typeof options ) {
      return this.defaultChildConfig({label: options})
    }
    return Object.assign({},this.config,{
      parent: this.config
    }, options)
  }

  createChild ( label:string|Config.ILoggerConfig ) {    
    const options = this.defaultChildConfig(label)
    return new Logger(options,this)
  }


}
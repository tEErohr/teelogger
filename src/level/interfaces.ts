import { LogLevel } from './types'
import { LogLevels } from './levels.enum'

export interface IPredicate {

  ( ):boolean

}

export interface ILogLevelChecker {

  ( level:LogLevel ):boolean

}

import { LogLevels } from './levels.enum'

export type LogLevel = string|keyof typeof LogLevels

export type LogLevelMap <T> = {
  [K in LogLevel]: T
}
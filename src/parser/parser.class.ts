import { IFormatOperator, IValueFormatter, operators } from '../operators'
import * as Style from '../style'
import { ILoggerConfig, ILoggerStyleMap, ILoggerStyleMaps } from '../config'
import { join, create, match } from '../regex'


export class Parser {

  constructor ( private operators:IFormatOperator[], styleMaps:Partial<ILoggerStyleMaps>={} ) {

    this.styleMaps = {
      operators: styleMaps.operators || {},
      types: styleMaps.types || {}
    }

  }

  private styleMaps:ILoggerStyleMaps

  protected regex=join(this.operators.map ( op => op.test ))

  protected testOperator ( operator:IFormatOperator, value:string ) {
    const re = create(operator.test)
    return re.test(value)
  }

  protected formatValueByType ( value:any, valueType:string ) {
    if ( valueType in this.styleMaps.types ) {
      const codes = this.styleMaps.types[valueType]
      value = Style.wrap(codes,value)
    }

    return value
  }

  protected formatValue ( value:any, operator:IFormatOperator ) {

    if ( operator.code in this.styleMaps.operators ) {
      const codes = this.styleMaps.operators[operator.code]
      value = Style.wrap(codes,value)
    }

    return value

  }

  parse ( pattern:string, args:any[] ) {

    return pattern.replace ( this.regex, ( value:string, ...params:any[] ) => {
      const operator = this.operators.find ( op => this.testOperator(op,value) )
      if ( !operator ) {
        throw new Error(`No operator for ${value}`)
      }

      //console.log( styles.wrap([1,3],'%s'), operator.test)
      //console.log( styles.mapCodes(44), value, styles.mapCodes(0))
      let arg = operator.noArg ? undefined : args.shift()
      let formatted = operator.format(arg,...params)
      formatted = this.formatValueByType(formatted,typeof arg)
      return this.formatValue(formatted, operator)

    } )    

  }
}

